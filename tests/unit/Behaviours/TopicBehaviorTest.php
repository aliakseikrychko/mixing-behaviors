<?php

namespace unit\InSided\Behaviour;

use InSided\Behaviour\Shared\Categorised;
use InSided\Behaviour\Shared\Replyable;
use InSided\Behaviour\Shared\Topic;
use InSided\Behaviour\Shared\VO\Author;
use InSided\Behaviour\Shared\VO\Category;
use InSided\Behaviour\Shared\VO\Reply;
use PHPUnit\Framework\TestCase;

final class TopicBehaviorTest extends TestCase
{
    public function testRepliable()
    {
        $alex = new Author('alex');
        $topic = new Topic($alex);

        $reply = new Reply($alex, 'the initial reply');
        $repliableTopic = new Replyable($topic, $reply);

        self::assertEquals([$reply], $repliableTopic->getReplies());
        self::assertEquals($alex, $repliableTopic->getAuthor());
    }

    public function testCategorised()
    {
        $alex = new Author('alex');
        $topic = new Topic($alex);

        $category = new Category('the only category');
        $categorisedTopic = new Categorised($topic, $category);

        self::assertEquals($category, $categorisedTopic->getCategory());
        self::assertEquals($alex, $categorisedTopic->getAuthor());
    }

    public function testReplyableAndCategorised()
    {
        $alex = new Author('alex');
        $topic = new Topic($alex);

        $category = new Category('the only category');
        $categorisedTopic = new Categorised($topic, $category);
        $reply = new Reply($alex, 'the initial reply');
        $repliableTopic = new Replyable($categorisedTopic, $reply);

        self::assertEquals([$reply], $repliableTopic->getReplies());
        self::assertEquals($category, $repliableTopic->getCategory());
        self::assertEquals($alex, $repliableTopic->getAuthor());
    }
}
