<?php

declare(strict_types = 1);

namespace InSided\ContentType;

use InSided\Author;
use InSided\Behaviour\Shared\Answerable;
use InSided\Behaviour\Shared\Categorised;
use InSided\Behaviour\Shared\IAnswerable;
use InSided\Behaviour\Shared\ICategorised;
use InSided\Behaviour\Shared\ITopic;
use InSided\Behaviour\Shared\Replyable;
use InSided\Behaviour\Shared\Topic;
use InSided\Behaviour\Shared\VO\Content;
use InSided\Category;
use InSided\Reply;

final class Question implements ITopic, ICategorised, IAnswerable
{
    protected $topic;

    public function __construct(Author $author, Content $content, Category $category, Reply ...$replies)
    {
        $this->topic = new Answerable(new Replyable(new Categorised(new Topic($author, $content), $category), $replies));
    }

    public function setCategory(Category $category): void
    {
        $this->topic->setCategory($category);
    }

    public function getCategory(): ?Category
    {
        return $this->topic->getCategory();
    }

    public function addReply(Reply $reply): void
    {
        $this->topic->addReply($reply);
    }

    public function trashReply(Reply $replyToRemove): void
    {
        $this->topic->trashReply($replyToRemove);
    }

    public function getReplies(): array
    {
        return $this->topic->getReplies();
    }

    public function getAuthor(): Author
    {
        return $this->topic->getAuthor();
    }

    public function setAuthor(Author $author): void
    {
        $this->topic->setAuthor($author);
    }

    public function isAnswered(): bool
    {
        return $this->topic->isAnswered();
    }

    public function getAnswer(): ?Reply
    {
        return $this->topic->getAnswer();
    }

    public function markAnswer(Reply $reply): void
    {
        $this->topic->markAnswer($reply);
    }
}
