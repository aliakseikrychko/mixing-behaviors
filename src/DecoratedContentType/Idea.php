<?php

declare(strict_types = 1);

namespace InSided\ContentType;

use InSided\Author;
use InSided\Behaviour\Shared\Categorised;
use InSided\Behaviour\Shared\ICategorised;
use InSided\Behaviour\Shared\Ideation;
use InSided\Behaviour\Shared\IIdeation;
use InSided\Behaviour\Shared\IReplyable;
use InSided\Behaviour\Shared\ITopic;
use InSided\Behaviour\Shared\Replyable;
use InSided\Behaviour\Shared\Topic;
use InSided\Behaviour\Shared\VO\Content;
use InSided\Behaviour\Shared\VO\Votes;
use InSided\Category;
use InSided\IdeaStatus;
use InSided\Reply;

final class Idea implements ITopic, IReplyable, ICategorised, IIdeation
{
    protected $topic;

    public function __construct(Author $author, Content $content, Category $category, Reply ...$replies)
    {
        $this->topic = new Ideation(new Replyable(new Categorised(new Topic($author, $content), $category), $replies));
    }

    public function setCategory(Category $category): void
    {
        $this->topic->setCategory($category);
    }

    public function getCategory(): ?Category
    {
        return $this->topic->getCategory();
    }

    public function addReply(Reply $reply): void
    {
        $this->topic->addReply($reply);
    }

    public function trashReply(Reply $replyToRemove): void
    {
        $this->topic->trashReply($replyToRemove);
    }

    public function getReplies(): array
    {
        return $this->topic->getReplies();
    }

    public function getAuthor(): Author
    {
        return $this->topic->getAuthor();
    }

    public function setAuthor(Author $author): void
    {
        $this->topic->setAuthor($author);
    }

    public function changeStatus(IdeaStatus $status): void
    {
        $this->topic->changeStatus($status);
    }

    public function getStatus(): IdeaStatus
    {
        return $this->topic->getStatus();
    }

    public function upVote(Author $author): Votes
    {
        return $this->topic->upVote($author);
    }

    public function downVote(Author $author): Votes
    {
        return $this->topic->downVote($author);
    }

    public function getVotes(): Votes
    {
        return $this->topic->getVotes();
    }

    public function getContent(): Content
    {
        return $this->topic->getContent();
    }

    public function setContent(Content $content): void
    {
        $this->topic->setContent($content);
    }
}
