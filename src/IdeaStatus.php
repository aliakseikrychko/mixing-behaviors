<?php


namespace InSided;


use Ramsey\Uuid\Uuid;

final class IdeaStatus
{
    private $id;
    private $status;

    public function __construct(string $status)
    {
        $this->id = Uuid::uuid4();
        $this->status = $status;
    }

    public static function default(): self
    {
        return new self('default status');
    }

    public function id(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->status;
    }
}
