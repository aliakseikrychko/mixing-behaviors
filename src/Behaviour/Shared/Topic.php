<?php


namespace InSided\Behaviour\Shared;


use InSided\Author;
use InSided\Behaviour\Shared\VO\Content;
use Ramsey\Uuid\Uuid;

final class Topic implements ITopic
{
    /**
     * @var Author
     */
    private $author;
    /**
     * @var Content
     */
    private $content;
    /**
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    public function __construct(Author $author, Content $content)
    {
        $this->id = Uuid::uuid4();
        $this->author = $author;
        $this->content = $content;
    }

    /**
     * @return Author
     */
    public function getAuthor(): Author
    {
        return $this->author;
    }

    /**
     * @param Author $author
     */
    public function setAuthor(Author $author): void
    {
        $this->author = $author;
    }

    public function getContent(): Content
    {
        return $this->content;
    }

    public function setContent(Content $content): void
    {
        $this->content = $content;
    }
}
