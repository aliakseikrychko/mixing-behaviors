<?php

declare(strict_types = 1);

namespace InSided\Behaviour\Shared;


use InSided\Author;
use InSided\Behaviour\Shared\VO\Content;
use InSided\Reply;

final class Replyable extends TopicBehavior implements ITopic, IReplyable
{
    protected $topic;
    private $replies;

    public function __construct(ITopic $topic, Reply ...$replies)
    {
        $this->topic = $topic;
        $this->replies = $replies;
    }

    public function addReply(Reply $reply): void
    {
        $this->replies[] = $reply;
    }

    public function trashReply(Reply $replyToRemove): void
    {
        array_values(array_filter($this->replies, function (Reply $reply) use ($replyToRemove) {
            return $reply->id() !== $replyToRemove->id();
        }));
    }

    public function getReplies(): array
    {
        return $this->replies;
    }

    public function getAuthor(): Author
    {
        return $this->topic->getAuthor();
    }

    public function setAuthor(Author $author): void
    {
        $this->topic->setAuthor($author);
    }

    public function getContent(): Content
    {
        return $this->topic->getContent();
    }

    public function setContent(Content $content): void
    {
        $this->topic->setContent($content);
    }
}
