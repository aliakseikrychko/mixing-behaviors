<?php

declare(strict_types = 1);

namespace InSided\Behaviour\Shared;

abstract class TopicBehavior
{
    protected $topic;

    public function __call(string $name, array $arguments)
    {
        return $this->topic->$name(...$arguments);
    }
}
