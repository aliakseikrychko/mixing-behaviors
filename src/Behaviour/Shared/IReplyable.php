<?php


namespace InSided\Behaviour\Shared;


use InSided\Reply;

interface IReplyable
{
    public function addReply(Reply $reply): void;

    public function trashReply(Reply $replyToRemove): void;

    /** @return Reply[] */
    public function getReplies(): array;
}
