<?php

namespace InSided\Behaviour\Shared;

use InSided\Author;
use InSided\Behaviour\Shared\VO\Votes;
use InSided\IdeaStatus;

interface IIdeation extends ITopic
{
    public function changeStatus(IdeaStatus $status): void;

    public function getStatus(): IdeaStatus;

    public function upVote(Author $author): Votes;

    public function downVote(Author $author): Votes;

    public function getVotes(): Votes;
}
