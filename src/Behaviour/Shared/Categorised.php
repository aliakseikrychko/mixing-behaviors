<?php

declare(strict_types = 1);

namespace InSided\Behaviour\Shared;

use InSided\Author;
use InSided\Behaviour\Shared\VO\Content;
use InSided\Category;

final class Categorised extends TopicBehavior implements ITopic, ICategorised
{
    protected $topic;
    private $category;

    public function __construct(ITopic $topic, Category $category = null)
    {
        $this->category = $category;
        $this->topic = $topic;
    }

    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function getAuthor(): Author
    {
        return $this->topic->getAuthor();
    }

    public function setAuthor(Author $author): void
    {
        $this->topic->setAuthor($author);
    }

    public function getContent(): Content
    {
        return $this->topic->getContent();
    }

    public function setContent(Content $content): void
    {
        $this->topic->setContent($content);
    }
}
