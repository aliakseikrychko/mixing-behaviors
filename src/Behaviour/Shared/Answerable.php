<?php

declare(strict_types = 1);

namespace InSided\Behaviour\Shared;

use InSided\Author;
use InSided\Behaviour\Shared\VO\Content;
use InSided\Reply;

final class Answerable extends TopicBehavior implements ITopic, IAnswerable
{
    protected $topic;
    private $answer;

    public function __construct(IReplyable $topic, Reply $answer = null)
    {
        $this->topic = $topic;
        $this->answer = $answer;
    }

    public function getAuthor(): Author
    {
        return $this->topic->getAuthor();
    }

    public function setAuthor(Author $author): void
    {
        $this->topic->setAuthor($author);
    }

    public function isAnswered(): bool
    {
        return $this->answer != null;
    }

    public function getAnswer(): ?Reply
    {
        return $this->answer;
    }

    public function markAnswer(Reply $reply): void
    {
        if (in_array($reply, $this->topic->getReplies()))
        $this->answer = $reply;
    }

    public function addReply(Reply $reply): void
    {
        $this->topic->addReply($reply);
    }

    public function trashReply(Reply $replyToRemove): void
    {
        $this->topic->trashReply($replyToRemove);
    }

    public function getReplies(): array
    {
        return $this->topic->getReplies();
    }

    public function getContent(): Content
    {
        return $this->topic->getContent();
    }

    public function setContent(Content $content): void
    {
        $this->topic->setContent($content);
    }
}
