<?php


namespace InSided\Behaviour\Shared\VO;


use InSided\Author;

final class Votes
{
    /**
     * @var \DateTimeImmutable[]
     */
    private $votes;

    public function __construct()
    {
        $this->votes = [];
    }

    public function upVote(Author $author): self
    {
        $this->votes[$author->id()] = new \DateTimeImmutable();

        return $this;
    }

    public function downVote(Author $author): self
    {
        if (isset($this->votes[$author->id()])) {
            unset($this->votes[$author->id()]);
        }

        return $this;
    }
}
