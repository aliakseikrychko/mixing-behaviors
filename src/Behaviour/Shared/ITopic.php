<?php

declare(strict_types = 1);

namespace InSided\Behaviour\Shared;


use InSided\Author;
use InSided\Behaviour\Shared\VO\Content;

interface ITopic
{
    public function getAuthor(): Author;

    public function setAuthor(Author $author): void;

    public function getContent(): Content;

    public function setContent(Content $content): void;
}
