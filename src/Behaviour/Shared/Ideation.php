<?php

declare(strict_types = 1);

namespace InSided\Behaviour\Shared;

use InSided\Author;
use InSided\Behaviour\Shared\VO\Content;
use InSided\Behaviour\Shared\VO\Votes;
use InSided\IdeaStatus;

final class Ideation extends TopicBehavior implements ITopic, IIdeation
{
    protected $topic;
    /**
     * @var IdeaStatus
     */
    private $ideaStatus;
    /**
     * @var Votes
     */
    private $votes;

    public function __construct(ITopic $topic, IdeaStatus $ideaStatus = null, Votes $votes = null)
    {
        $this->topic = $topic;
        $this->ideaStatus = $ideaStatus ?? IdeaStatus::default();
        $this->votes = $votes ?? new Votes();
    }

    public function getAuthor(): Author
    {
        return $this->topic->getAuthor();
    }

    public function setAuthor(Author $author): void
    {
        $this->topic->setAuthor($author);
    }

    public function changeStatus(IdeaStatus $status): void
    {
        $this->ideaStatus = $status;
    }

    public function getStatus(): IdeaStatus
    {
        return $this->ideaStatus;
    }

    public function upVote(Author $author): Votes
    {
        return $this->votes->upVote($author);
    }

    public function downVote(Author $author): Votes
    {
        return $this->votes->downVote($author);
    }

    public function getVotes(): Votes
    {
        return $this->votes;
    }

    public function getContent(): Content
    {
        return $this->topic->getContent();
    }

    public function setContent(Content $content): void
    {
        $this->topic->setContent($content);
    }
}
