<?php


namespace InSided\Behaviour\Shared;


use InSided\Category;

interface ICategorised
{
    public function setCategory(Category $category): void;

    public function getCategory(): ?Category;
}
