<?php

namespace InSided\Behaviour\Shared;

use InSided\Reply;

interface IAnswerable extends IReplyable
{
    public function isAnswered(): bool;

    public function getAnswer(): ?Reply;

    public function markAnswer(Reply $reply): void;
}
