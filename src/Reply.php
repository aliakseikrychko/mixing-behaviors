<?php


namespace InSided;


use InSided\Behaviour\Shared\VO\Content;
use Ramsey\Uuid\Uuid;

final class Reply
{
    private $id;
    /**
     * @var Author
     */
    private $author;
    /**
     * @var Content
     */
    private $content;
    /**
     * @var \DateTimeImmutable
     */
    private $repliedAt;

    public function __construct(Author $author, Content $content)
    {
        $this->id = Uuid::uuid4();
        $this->author = $author;
        $this->content = $content;
        $this->repliedAt = new \DateTimeImmutable();
    }

    public function id(): string
    {
        return $this->id;
    }
}
