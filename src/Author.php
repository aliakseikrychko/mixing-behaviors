<?php


namespace InSided;


use Ramsey\Uuid\Uuid;

final class Author
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    public function __construct(string $name)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name;
    }

    public function id(): string
    {
        return $this->id;
    }
}
